const fs = require("fs");
const path = require("path");

function directory(folderPath, randomNumber) {
  fs.mkdir("createdFolder", () => {
    console.log("directory created");
    for (let count = 0; count <= randomNumber; count++) {
      fs.writeFile(path.join(__dirname, `${folderPath}/file${count}.json`), `File${ count} created`, (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log(`file${count}.json created`);
          fs.unlink(path.join(__dirname, `${folderPath}/file${count}.json`), (err) => {
            if (err) {
              console.error(err);
            } else {
              console.log(`file${ count} Deleted`);
            }
          });
        }
      }
      );
    }
  })
}

module.exports = directory;



