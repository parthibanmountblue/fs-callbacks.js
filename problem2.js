const fs = require("fs")

const path = require("path");


//  1. Read the given file lipsum.txt
function fileWork() {
  fs.readFile(path.join(__dirname, "./lipsum.txt"), "utf-8", function (err, data) {
    if (err) {
      console.error(err);
    } else {
      console.log("lipsum.txt file read")

      // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

      let result = data.toUpperCase();         // convert to uppper case
      fs.writeFile(path.join(__dirname, "./output.json"), JSON.stringify(result), function (err) {             // storing output
        if (err) {
          console.error(err);
        }
        console.log("file changed to uppercase and stored in output.json file")
        fs.writeFile(path.join(__dirname, "filenames.txt"), "output.json\n", (err) => {            // filename containing all file names
          if (err) {
            console.error(err);
          }
          //   3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

          fs.readFile(path.join(__dirname, "./output.json"), "utf-8", (err, data) => {         // reading uppercased file
            if (err) {
              console.error(err);
            } else {
              console.log("uppercase new file read.....")
              let result = data.toLowerCase();
              console.log("converted to lower case .....")
              let sentenceSplit = result.split('.')
              console.log("sentence split")
  
              fs.writeFile(path.join(__dirname, 'sentenceSplit.txt'), JSON.stringify(sentenceSplit), (err) => {   // converting lower case and spliting it in sentence and writing in file
                if (err) {
                  console.error(err)
                }
                console.log("lower case file stored in sentence split.....")
                fs.appendFile(path.join(__dirname, 'filenames.txt'), "sentenceSplit.txt\n", (err) => {   //filename containing all files name
                  if (err) {
                    console.error(err)
                  }

                  // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

                  fs.readFile(path.join(__dirname, './sentenceSplit.txt'), 'utf-8', (err, data) => {// reading split sentence array
                    if (err) {
                      console.error(err)
                    } else {
                      let result = JSON.parse(data)
                      let sortedArray = result.sort(
                        (p1, p2) =>
                          (p1[1] > p2[1]) ? 1 : (p1[1] < p2[1]) ? -1 : 0);
                      console.log("sorting is executed...")
          
                      fs.writeFile(path.join(__dirname, 'sorted.txt'), JSON.stringify(sortedArray), (err) => {
                        if (err) {
                          console.error(err)
                        }
                      })
                      fs.appendFile(path.join(__dirname, 'filenames.txt'), "sorted.txt", (err) => {
                        if (err) {
                          console.error(err)
                        }
                      })

                      //    5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

                      fs.readFile(path.join(__dirname, './filenames.txt'), 'utf-8', (err, data) => {
                        if (err) {
                          console.error(err)
                        }
                        else {
                          console.log("reading of filenames.txt is started ....")
                          let result = data.split("\n")
                          result.forEach(filename => {
                            fs.unlink(path.join(__dirname, `/${filename}`), (err) => {
                              if (err) {
                                console.error(err)
                              }
                              console.log(`file deleted ${filename}`)
                            })
                          })
                        }
                      })
                    }
                  })
                })
              })
            }
          });
        });
      });
    }
  });
}

module.exports = fileWork

